const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const resolve = data => path.resolve(__dirname, data);

module.exports = {

  mode: 'development',

  devtool: 'inline-source-map',

  resolve: {
    // 路径别名
    alias: {
      api: resolve('src/api'),
      components: resolve('src/components'),
      pages: resolve('src/pages'),
      fonts: resolve('src/assets/fonts'),
      images: resolve('src/assets/images'),
      styles: resolve('src/assets/styles'),
    },
  },

  entry: './src/pages/index/index.js',

  output: {
    path: resolve('dist'),
    filename: 'index.js',
    // 静态资源模块文件名
    assetModuleFilename: 'assets/[name][ext][query]',
    // 打包的时候打开，清除dist目录，开发环境下注释掉，会报错
    // clean:true
  },

  module: {
    rules: [
      {
        test: /\.art$/i,
        loader: 'art-template-loader'
      },
      {
        test: /\.css$/i,
        use: [MiniCssExtractPlugin.loader, 'css-loader']
      },
      {
        test: /\.(png|svg|jpg|jpeg|gif)$/i,
        type: 'asset',
        // 控制图片是否转化为base64编码格式
        parser: {
          dataUrlCondition: {
            maxSize: 4 * 1024 // 4kb,默认值是8kb
          }
        }
      },
      {
        test: /\.(woff|woff2|eot|ttf|otf)$/i,
        type: 'asset',
        parser: {
          dataUrlCondition: {
            maxSize: 4 * 1024 // 4kb,默认值是8kb
          }
        }
      },
    ]
  },

  plugins: [

    new MiniCssExtractPlugin({
      filename: 'style.css'
    }),

    new HtmlWebpackPlugin({
      template: './src/pages/index/index.art',
      filename: 'index.html',
      minify: {
        // 删除 index.html 中的注释
        removeComments: true,
        // 删除 index.html 中的空格
        collapseWhitespace: true,
        // 删除各种 html 标签属性值的双引号
        removeAttributeQuotes: true
      }
    })

  ]


}