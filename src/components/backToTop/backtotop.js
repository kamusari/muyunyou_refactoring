/* 
 *  返回顶部按钮动画特效
 *  开发：Amron
 *  日期：6-18
 */

// 获取需要的元素
const btn = document.getElementById('toTop');

const root = document.documentElement;

let lock = false;

let timer;

btn.onclick = () => {

  // 函数节流判断
  if (lock) return;

  clearInterval(timer);

  // 利用定时器设置动画
  timer = setInterval(function () {
    root.scrollTop -= 100;
    // 判断位置，关闭定时器
    if (root.scrollTop <= 0) clearInterval(timer);
  }, 10)

  // 上锁
  lock = true;
  // 开锁
  setTimeout(function () {
    lock = false;
  }, 1000)
}

// 添加滚动事件监听
window.onscroll = () => {
  // 当页面往下滚动超过屏幕高度之后显示返回顶部按钮
  if (root.scrollTop >= root.clientHeight) btn.style.display = 'block';
  else btn.style.display = 'none';
}
