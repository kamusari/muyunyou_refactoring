/* 
 *  api接口
 *  开发：Amron
 *  日期：6-18
 */

/* 验证码6-18生成，有效期30天 */
export const CODE = 'J45A8CD51D7E8FBB0';

/* banner接口 */
export const BANNER = 'https://www.imooc.com/api/mall-PC/index/slider?icode=';

/* 一级菜单 */
export const MAIN_MENU = 'https://www.imooc.com/api/mall-PC/index/menu';

/* 二级菜单 */
export const SUB_MENU = [
  'https://www.imooc.com/api/mall-PC/index/menu/hot',
  'https://www.imooc.com/api/mall-PC/index/menu/hk',
  'https://www.imooc.com/api/mall-PC/index/menu/japan',
  'https://www.imooc.com/api/mall-PC/index/menu/asia',
  'https://www.imooc.com/api/mall-PC/index/menu/eu',
  'https://www.imooc.com/api/mall-PC/index/menu/au'
];

/* 广告 */
export const ADVERTISE = 'https://www.imooc.com/api/mall-PC/index/ad';

/* 新鲜甩尾区域 */
export const FRESH = 'https://www.imooc.com/api/mall-PC/index/seckill';

/* 机酒自由行 */
export const TRAVEL = 'https://www.imooc.com/api/mall-PC/index/self_guided_tour?icode=';

/* 当地玩乐 */
export const LOCAL = 'https://www.imooc.com/api/mall-PC/index/local_fun';

/* 特色体验 */
export const FEATURE = 'https://www.imooc.com/api/mall-PC/index/local_exp';

/* 最世界 */
export const WORLD = 'https://www.imooc.com/api/mall-PC/index/special_subject';

/* 跟团 */
export const FOLLOW = 'https://www.imooc.com/api/mall-PC/index/group_tour';