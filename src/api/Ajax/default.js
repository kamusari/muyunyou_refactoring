/* 
 *  HTTP请求配置默认值
 *  开发：Amron
 *  日期：6-18
 */

import { HTTP_POST_REQUEST } from "./constants"

export const AJAX_OPTIONS_DEFAULT = {

  // 请求方法
  method: HTTP_POST_REQUEST,

  // 返回值类型
  responseType: '',

  // 超时时间
  timeoutTime: 0,

  // 是否携带cookie
  withCredential: false,

  // 请求头的字段，表明请求体中的数据类型
  contentType: '',

  // 请求头中的传参
  params: null,

  // 请求体中的数据
  data: null,

  // 请求中止
  abort() { },

  // 请求成功
  success() { },

  // 请求失败
  requestError() { },

  // 响应失败
  responseError() { },

  // 请求超时
  timeout() { },
}