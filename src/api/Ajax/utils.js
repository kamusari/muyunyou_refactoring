/* 
 *  HTTP请求工具类
 *  开发：Amron
 *  日期：6-18
 */

// 将对象形式数据转化为表单格式的字符串
export const objToForm = (obj) => {

  if (!obj) return '';

  let result = '';

  const tempArr = [];

  for (const [key, value] of Object.entries(obj)) {
    tempArr.push(`${key}=${value}`);
  }

  return result += tempArr.join('&');
}

// 判断url上面是添加？还是&
export const spliceURL = (url, obj) => {
  if (!obj || Object.keys(obj).length === 0) return url;
  return url + (url.includes('?') ? '&' : '?') + objToForm(obj);
}

