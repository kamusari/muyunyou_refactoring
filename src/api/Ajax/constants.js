/* 
 *  HTTP请求常量
 *  开发：Amron
 *  日期：6-18
 */

export const HTTP_GET_REQUEST = 'GET';
export const HTTP_POST_REQUEST = 'POST';
export const CONTENT_TYPE_FORM_URLENCODED = 'application/x-www-form-urlencoded';
export const CONTENT_TYPE_JSON = 'application/json';

