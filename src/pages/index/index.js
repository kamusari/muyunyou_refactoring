
/***************导入样式***************/
// 公共部分
import 'styles/reset.css';
import 'styles/base.css';
// 首页部分
import './index.css';


/***************导入组件***************/
// 公共部分
import 'components/topbar'
import 'components/main-nav'
import 'components/sub-nav'
import 'components/search'
import 'components/backToTop'
import 'components/slogan'
import 'components/loading'
// 首页部分
import './components/slider'
import './components/menu'
import './components/ads'
import './components/daily'
import './components/travel'
import './components/local'
import './components/feature'
import './components/world'
import './components/follow'