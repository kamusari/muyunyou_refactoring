import './travel.css'

import { getJson } from 'api/Ajax';
import { TRAVEL, CODE } from 'api/config';

import render from './travel.art'

const box = document.querySelector('section.travel div.content')

// 发送Ajax请求
getJson(TRAVEL + CODE)
  .then(data => {
    box.innerHTML = render(data);
  })
  .catch(error => {
    console.log(error);
  })

