
import { getJson } from 'api/Ajax'
import { MAIN_MENU } from 'api/config'

import render from './menu.art'

import Menu from './modules/baseMenu'

const main = document.getElementById('mainMenuBox');
const location = document.getElementById('location');

// 获取一级菜单数据
getJson(MAIN_MENU)
  .then(data => {
    main.innerHTML = render({ data });
    new Menu(location);
  })
  .catch(error => {
    console.log(error);
  })
