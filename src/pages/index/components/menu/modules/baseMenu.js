/* 
 *  菜单类
 *  开发：Amron
 *  日期：6-18
 */

import { getJson } from 'api/Ajax';
import { SUB_MENU } from "api/config";
import { LOADING_PARAMS, CURRENT_LI, TRANSPARENT_BORDER } from './constants';

import renderData from '../subMenu.art';
import renderLoad from 'components/loading/loading.art';

class Menu {
  constructor(el) {
    // 添加需要的元素节点
    this.menu = el;
    this.mainMenu = el.querySelector('#mainMenu');
    this.subMenu = el.querySelector('#subMenu');

    // 二级菜单模板
    this.subMenuData = [];
    // 当前鼠标所在的序号
    this.mouseNumber = '';

    this.bindEvents();

  }

  bindEvents() {

    // 获得一级菜单的所有li
    const lis = this.mainMenu.querySelectorAll('li');

    for (const item of lis) {
      // 给每个li添加鼠标进入事件监听
      item.addEventListener('mouseenter', async () => {
        // 获取当前鼠标索引
        this.mouseNumber = item.dataset.n;
        // 改变li的背景样式
        this.changeCurrent(lis);
        // 改变li的边框样式
        this.addBorderStyle(item);
        // 发起请求 使用await避免速度太快，二级菜单显示不出来
        await this.getData(item);
      });
      // 给每个li添加鼠标移出事件监听
      item.addEventListener('mouseleave', () => {
        // 恢复二级菜单的加载图标，要不然第二个加载的时候，先看到的就是第一个的数据
        this.subMenu.innerHTML = renderLoad(LOADING_PARAMS);
        // 改变li的边框样式
        this.removeBorderStyle(item);
      })
    }

    // 给二级菜单添加鼠标移入事件监听
    this.subMenu.addEventListener('mouseenter', () => {
      this.changeCurrent(lis);
      if (lis[this.mouseNumber].dataset)
        this.subMenu.innerHTML = this.subMenuData[this.mouseNumber];
    })

    // 给整个菜单添加鼠标移出事件监听
    this.menu.addEventListener('mouseleave', () => {
      for (const item of lis) item.classList.remove(CURRENT_LI);
    })

    // 阻断事件的传播，目的是为了鼠标在菜单上时不影响轮播图的自动播放
    this.menu.addEventListener('mouseover', e => {
      e.stopPropagation();
    })
    this.menu.addEventListener('mouseout', e => {
      e.stopPropagation();
    })

  }

  // 发送Ajax请求
  getData(el) {
    // 判断是不是第一次
    if (el.dataset.done) {
      this.subMenu.innerHTML = this.subMenuData[el.dataset.n];
      return;
    }
    return getJson(SUB_MENU[el.dataset.n])
      .then(data => {
        // 渲染模板到页面上
        this.subMenu.innerHTML = renderData({ data });
        // 同时将数据进行保存
        this.subMenuData[el.dataset.n] = renderData({ data });
        // 设置done属性，作为已经发送过请求的标志
        el.dataset.done = true;
      })
      .catch(error => {
        console.log(error);
      })
  }

  // 改变li的背景样式
  changeCurrent(lis) {
    for (const item of lis) {
      if (item.dataset.n === this.mouseNumber)
        item.classList.add(CURRENT_LI);
      else
        item.classList.remove(CURRENT_LI);
    }
  }

  // 改变li的边框样式
  addBorderStyle(li) {
    li.classList.add(TRANSPARENT_BORDER);
    if (li.previousElementSibling) li.previousElementSibling.classList.add(TRANSPARENT_BORDER);
  }
  removeBorderStyle(li) {
    li.classList.remove(TRANSPARENT_BORDER);
    if (li.previousElementSibling) li.previousElementSibling.classList.remove(TRANSPARENT_BORDER);
  }

}

export default Menu