
import './ad.css';

import { getJson } from 'api/Ajax';
import { ADVERTISE } from 'api/config';

import render from './ad.art'

const box = document.querySelector('aside>div')

getJson(ADVERTISE)
  .then(data => {
    box.innerHTML = render({ data });
  })
  .catch(error => {
    console.log(error);
  })