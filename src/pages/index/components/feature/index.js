import './feature.css'

import { getJson } from 'api/Ajax';
import { FEATURE } from 'api/config';

import render from './feature.art'

const box = document.querySelector('section.feature div.comment')

// 发送Ajax请求
getJson(FEATURE)
  .then(data => {
    box.innerHTML = render({ data });
  })
  .catch(error => {
    console.log(error);
  })
