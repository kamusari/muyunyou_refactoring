
import './daily.css'

import { getJson } from 'api/Ajax';
import { FRESH } from 'api/config';

import render from './daily.art'

const box = document.querySelector('section.daily div.content')

// 发送Ajax请求
getJson(FRESH)
  .then(data => {
    box.innerHTML = render({ data });
  })
  .catch(error => {
    console.log(error);
  })