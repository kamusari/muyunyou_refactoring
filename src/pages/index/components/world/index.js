import './world.css'

import { getJson } from 'api/Ajax';
import { WORLD } from 'api/config';

import render from './world.art'

const box = document.querySelector('section.world div.content')

// 发送Ajax请求
getJson(WORLD)
  .then(data => {
    box.innerHTML = render(data);
  })
  .catch(error => {
    console.log(error);
  })
