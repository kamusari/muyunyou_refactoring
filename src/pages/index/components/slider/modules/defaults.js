/* 
 *  轮播图配置默认值
 *  开发：Amron
 *  日期：6-18
 */

export default {
  // 初始索引
  initialIndex: 0,
  // 切换时是否有动画
  animation: true,
  // 切换速度，单位 ms
  speed: 300,
  // 是否自动
  autoPlay: true
};
