/* 
 *  轮播图类
 *  开发：Amron
 *  日期：6-18
 */

// 默认参数
import DEFAULTS from './defaults';

// 常量
import { ELEMENT_NODE } from './constants';

class BaseSlider {
  constructor(el, options) {
    if (el.nodeType !== ELEMENT_NODE)
      throw new Error('实例化的时候，请传入 DOM 元素！');

    const sliderContent = el.querySelector('.slider_content');
    const sliderItems = sliderContent.querySelectorAll('.slider_item');

    // 实际参数
    this.options = Object.assign({}, DEFAULTS, options);
    // 添加到this上，为了在方法中使用
    this.slider = el;
    this.sliderContent = sliderContent;
    this.sliderItems = sliderItems;
    this.minIndex = 0;
    this.number = sliderItems.length;
    this.maxIndex = sliderItems.length - 1;
    this.currIndex = this.getCorrectedIndex(this.options.initialIndex);

    // 每个slider_item的宽度（每次移动的距离）
    this.itemWidth = sliderContent.offsetWidth;
    this.init();
    this.changeWidth();
  }

  // 当页面宽度发生变化时，重新初始化
  changeWidth() {
    window.addEventListener('resize', () => {
      this.itemWidth = document.documentElement.offsetWidth;
      this.setAnimationSpeed(0);
      this.init();
    })
  }

  // 获取修正后的索引值
  getCorrectedIndex(index) {
    if (index < this.minIndex) return this.maxIndex;
    if (index > this.maxIndex) return this.minIndex;
    return index;
  }

  // 初始化
  init() {
    // 为每个slider_item设置宽度
    this.setItemsWidth();

    // 为slider_content设置宽度
    this.setContentWidth();

    // 切换到初始索引initialIndex
    this.move(this.getDistance());
  }

  // 为每个slider_item设置宽度
  setItemsWidth() {
    for (const item of this.sliderItems) {
      item.style.width = `${this.itemWidth}px`;
    }
  }

  // 为slider_content设置宽度
  setContentWidth() {
    this.sliderContent.style.width = `${this.itemWidth * this.number}px`;
  }

  // 不带动画的移动
  move(distance) {
    this.sliderContent.style.transform = `translate3d(${distance}px, 0px, 0px)`;
  }

  // 带动画的移动
  moveWithAnimation(distance) {
    this.setAnimationSpeed(this.options.speed);
    this.move(distance);
  }

  // 设置切换动画速度
  setAnimationSpeed(speed) {
    this.sliderContent.style.transitionDuration = `${speed}ms`;
  }

  // 获取要移动的距离
  getDistance(index = this.currIndex) {
    return -this.itemWidth * index;
  }

  // 切换到index索引对应的幻灯片
  to(index) {
    index = this.getCorrectedIndex(index);
    if (this.currIndex === index) return;

    this.currIndex = index;
    const distance = this.getDistance();

    if (this.options.animation) {
      return this.moveWithAnimation(distance);
    } else {
      return this.move(distance);
    }
  }

  // 切换上一张
  prev() {
    this.to(this.currIndex - 1);
  }

  // 切换下一张
  next() {
    this.to(this.currIndex + 1);
  }

  // 获取当前索引
  getCurrIndex() {
    return this.currIndex;
  }
}

export default BaseSlider;
