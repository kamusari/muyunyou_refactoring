
// 导入轮播图的类
import ButtonSlider from './modules/buttonSlider';

// 导入Ajax请求方法
import { getJson } from 'api/Ajax/';
import { CODE, BANNER } from 'api/config'

// 导入slider模板函数
import render from './slider.art'


const box = document.getElementById('slider_box');
const banner = document.getElementById('carousel');


getJson(BANNER + CODE)
  .then(data => {
    // 将模板和数据渲染到首页上
    box.innerHTML = render({ data });

    // 获取slider类所需要的元素节点
    const slider = document.getElementById('slider');
    const btn = document.getElementById('button');
    const circles = document.getElementById('circles');

    // 实例化
    const obj = new ButtonSlider(slider, {
      // 初始索引
      initialIndex: 0,
      // 切换时是否有动画
      animation: true,
      // 切换速度，单位 ms
      speed: 300,
      // 自动滚动
      autoPlay: true
    }, btn, circles);

    // 给整个轮播图区域添加移入移出事件监听
    banner.addEventListener('mouseover', () => {
      clearInterval(obj.timer);
    });
    banner.addEventListener('mouseout', () => {
      if (obj.options.autoPlay) obj.setAuto();
    })

  })
  .catch((error) => {
    console.log(error);
  })