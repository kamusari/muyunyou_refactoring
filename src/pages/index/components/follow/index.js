import './follow.css'

import { getJson } from 'api/Ajax';
import { FOLLOW } from 'api/config';

import render from './follow.art'

const box = document.querySelector('section.follow div.content')

// 发送Ajax请求
getJson(FOLLOW)
  .then(data => {
    // 数据重构
    [data.one.url, data.items[0].url] = [data.items[0].url, data.one.url]
    box.innerHTML = render(data);
  })
  .catch(error => {
    console.log(error);
  })