import './local.css'

import { getJson } from 'api/Ajax';
import { LOCAL } from 'api/config';

import render from './local.art'

const box = document.querySelector('section.local div.content')

// 发送Ajax请求
getJson(LOCAL)
  .then(data => {
    // 重写数据结构
    const obj = {};
    obj.left = {
      one: data.one,
      item: data.items[0]
    };
    obj.middle = [data.items[1], data.items[2], data.items[3]],
      obj.right = {
        more: data.more,
        item: data.items[4]
      }
    box.innerHTML = render(obj);
  })
  .catch(error => {
    console.log(error);
  })
